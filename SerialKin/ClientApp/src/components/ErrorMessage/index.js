import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

function ErrorMessage(props) {
  return (
    <section className="error-message">
      {props.message}
    </section>
  );
}

ErrorMessage.propTypes = {
  message: PropTypes.string.isRequired
};

export default ErrorMessage;

import React from 'react';
import Icon from 'react-icons-kit';
import { rotateRight } from 'react-icons-kit/fa/rotateRight';
import './styles.scss';

function Loading() {
  return (
    <div className="loading">
      <Icon className="icon" size={30} icon={rotateRight} />
      <span>Загрузка</span>
    </div>
  );
}

export default Loading;

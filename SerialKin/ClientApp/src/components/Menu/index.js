import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classnames from 'classnames';
import Icon from 'react-icons-kit';
import { thList } from 'react-icons-kit/fa/thList';
import { hashtag } from 'react-icons-kit/fa/hashtag';
import { switchMenu } from '../../redux/actionCreators/system';
import './styles.scss';

function Menu(props) {
  const { isMenuOpen, switchMenu } = props;
  return (
    <nav
      className={classnames(
        'main-menu',
        isMenuOpen ? 'open' : ''
      )}
    >
      <ul>
        <li className="title">
          <Icon size={27} icon={thList} />
          <span>Меню</span>
        </li>
        <li className="item">
          <Link to="/" onClick={() => switchMenu()}>Главная страница</Link>
        </li>
        <li className="item">
          <Link to="/multserials" onClick={() => switchMenu()}>Мультсериалы</Link>
        </li>
        <li className="title">
          <Icon size={27} icon={hashtag} />
          <span>Теги</span>
        </li>
        <li className="item">
          <Link to="/fox" onClick={() => switchMenu()}>#FOX</Link>
        </li>
        <li className="item">
          <Link to="/cc" onClick={() => switchMenu()}>#COMEDY CENTRAL</Link>
        </li>
        <li className="item">
          <Link to="/cn" onClick={() => switchMenu()}>#CARTOON NETWORK</Link>
        </li>
        <li className="item">
          <Link to="/netflix" onClick={() => switchMenu()}>#NETFLIX</Link>
        </li>
      </ul>
    </nav>
  );
}

Menu.propTypes = {
  isMenuOpen: PropTypes.bool.isRequired,
  switchMenu: PropTypes.func.isRequired
};

const mapStateToProps = ({ system: { isMenuOpen } }) => ({ isMenuOpen });

const mapDispatchToProps = dispatch => bindActionCreators({
  switchMenu
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Menu);

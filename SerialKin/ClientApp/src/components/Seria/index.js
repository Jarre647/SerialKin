import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './styles.scss';
import Button from '../Button';


function Seria(props) {
  const { image, name, link } = props;
  return (
    <article className="seria">
      <img src={image} alt={name} />
      <div>
        <h1 className="title">{name}</h1>
        <div>
          <Link to={link}>
            <Button>
              Смотреть
            </Button>
          </Link>
        </div>
      </div>
    </article>
  );
}

Seria.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired
};

export default Seria;

import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import tags from './tags';
import serials from './serials';
import Header from '../../components/Header';
import Menu from '../../components/Menu';
import Main from '../Main';
import Serial from '../Serial';
import Season from '../Season';

function App() {
  return (
    <Fragment>
      <Header />
      <div className="wrapper">
        <Menu />
        <div className="content">
          <Switch>
            <Route exact path="/" component={Main} />
            <Route path="/multserials" render={() => <Main type="mult" />} />

            { // tags
              tags.map(
                (tag, i) =>
                  <Route key={i} path={`/${tag}`} render={() => <Main type={tag} />} />
              )
            }
            
            { // Serials
              serials.map(
                serial =>
                  <Route exact key={serial.key} path={`/${serial.name}`} render={() => <Serial serialName={serial.name} />} />
              )
            }

            { // seasons
              serials.map(
                serial =>
                  <Route key={serial.key} path={`/${serial.name}/:season`} render={props => <Season serialName={serial.name} title={serial.title} {...props} />} />
              )
            }
          </Switch>
        </div>
      </div>
    </Fragment>
  );
}

export default App;

export default [
  {
    key: 0,
    name: 'rick-and-morty',
    title: 'Рик и Морти'
  },
  {
    key: 1,
    name: 'simpsons',
    title: 'Симпсоны'
  },
  {
    key: 2,
    name: 'family-guy',
    title: 'Гриффины'
  },
  {
    key: 3,
    name: 'futurama',
    title: 'Футурама'
  },
  {
    key: 4,
    name: 'adventure-time',
    title: 'Время приключений'
  },
  {
    key: 5,
    name: 'disenchantment',
    title: 'Разочарование'
  },
  {
    key: 6,
    name: 'south-park',
    title: 'Южный парк'
  }
];

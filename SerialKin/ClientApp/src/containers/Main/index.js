import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import './styles.scss';
import Card from '../../components/Card';
import ErrorMessage from '../../components/ErrorMessage';
import Loading from '../../components/Loading';
import { addSerials } from '../../redux/actionCreators/serials';

function Main(props) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const { serials: allSerials, addSerials, type } = props;
  let serials = allSerials;

  useEffect(() => {
    if (!serials && !error) {
      axios
      .get('/api/serials')
      .then(({ data }) => addSerials(data))
      .then(() => setLoading(false))
      .catch(error => {
        console.error(error);
        setError(true);
      });
    }
    else setLoading(false);
  }, [addSerials, serials, setLoading, setError]);

  if (type && serials) {
      if (type === 'mult') serials = allSerials.filter(serial => serial.cartoon && true);
    else serials = allSerials.filter(serial => serial.author === type);
  }

  return (
    <section className="main-page">
      <h1 className="title">Библиотека сериалов</h1>
      {
        error ?
          <ErrorMessage message="Возникла ошибка при загрузке данных. Пожалуйста, перезагрузите страницу." /> :
          loading ?
            <Loading /> :
            <div className="cards">
              {
                serials.map(serial => <Card key={serial.id} data={serial} />)
              }
            </div>
      }
    </section>
  );
}

Main.propTypes = {
  serials: PropTypes.array,
  addSerials: PropTypes.func.isRequired,
  type: PropTypes.string
};

const mapStateToProps = ({ serials }) => ({ serials });

const mapDispatchToProps = dispatch => bindActionCreators({
  addSerials
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Main);

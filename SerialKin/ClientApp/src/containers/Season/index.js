import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import Player from 'react-player';
import Icon from 'react-icons-kit';
import { play } from 'react-icons-kit/fa/play';
import { setSeason } from '../../redux/actionCreators/season';
import { addSerials } from '../../redux/actionCreators/serials';
import './styles.scss';
import Loading from '../../components/Loading';
import ErrorMessage from '../../components/ErrorMessage';

function Season(props) {
  const [loading, setLoading] = useState(true);
  const [currentSeria, setSeria] = useState(1);
  const [link, setLink] = useState('');
  const [error, setError] = useState(false);

  const {
    title,
    serialName,
    season,
    setSeason,
    serials,
    addSerials
  } = props;

  const { season: seasonNumber } = props.match.params;  // Номер сезона
  const { url } = props.match;

  useEffect(() => {
    if (!serials && !error) {
      axios.get('/api/serials')
        .then(({ data }) => addSerials(data))
        .catch(error => {
          console.error(error);
          setError(true);
        });
    } else if (serials && !error) {
      const { id } = serials.find(serial => serial.title === title);

      axios.get(`/api/serials/${id}/${seasonNumber}`)
        .then(({ data }) => setSeason(data))
        .then(() => setLoading(false))
        .catch(error => {
          console.error(error);
          setError(true);
        });
    }
  }, [loading, url, setSeason, serials, addSerials, seasonNumber, title]);

  return (
    <section className="season">
      {
        error ?
          <ErrorMessage message="Возникла ошибка при загрузке данных. Пожалуйста, перезагрузите страницу." />:
          loading ?
            <Loading /> :
            <div>
            <section className="breadcrumbs">
              <Link to="/">Главная</Link>
              <span>-</span>
              <Link to={`/${serialName}`}>{title}</Link>
              <span>-</span>
              <Link to={url}>Сезон {seasonNumber}</Link>
            </section>
            <div>
              <div className="container">
                <div className="main-content">
                  <Player
                    url="/video/olya.mp4"
                    controls
                    height="auto"
                    className="player"
                  />
                  <section className="block">
                    <h2 className="title">
                      Комментарии
                    </h2>
                    <section className="no-content">
                      Комментариев пока нет.
                    </section>
                  </section>
                </div>

                <div className="side">
                  <section className="block">
                    <h2 className="title">Список серий</h2>
                    <section className="list">
                      {
                        season.map(
                          ({ numberSeries: seria, id, link }) =>
                            <article
                              className={`seria ${currentSeria === seria && 'active'}`}
                              key={id}
                              onClick={() => {setSeria(seria); setLink(link)}}
                            >
                              { currentSeria === seria && <Icon className="icon" icon={play} /> }
                              Серия {seria}
                            </article>
                        )
                      }
                    </section>
                  </section>

                  <section className="block">
                    <h2 className="title">
                      Другие сериалы
                    </h2>
                    <section className="list see-also">
                      {
                        serials &&
                          serials.map(
                            serial =>
                              serial.name === serialName ? null :
                              <article className="see-also-serial" key={serial.id}>
                                <Link to={serial.link}>
                                  <img alt={serial.title} src={serial.logo} width="100%" />
                                </Link>
                              </article>
                          )
                      }
                    </section>
                  </section>
                </div>
              </div>
            </div>
          </div>
      }
    </section>
  );
}

Season.propTypes = {
  serials: PropTypes.array,
  addSerials: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  serialName: PropTypes.string.isRequired,
  season: PropTypes.array.isRequired,
  setSeason: PropTypes.func.isRequired
};

const mapStateToProps = ({ season, serials }) => ({ season, serials });

const mapDispatchToProps = dispatch => bindActionCreators({
  setSeason,
  addSerials
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Season);

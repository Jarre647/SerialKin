import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './styles.scss';
import { addSerials } from '../../redux/actionCreators/serials';
import Loading from '../../components/Loading';
import Button from '../../components/Button';
import ErrorMessage from '../../components/ErrorMessage';

function Serial(props) {
  const [loading, setLoading] = useState(true);
  const [serial, setSerial] = useState(null);
  const [error, setError] = useState(false);

  const {
    serialName,
    serials,
    addSerials
  } = props;
  

  function getSeasons(serial) {
    const seasons = [];
    for(let i = 0; i < serial.seasons; i++) {
      seasons.push(<Link key={Math.random()} className="select-season-button" to={`${serial.link}/${i + 1}`}>Сезон {i + 1}</Link>);
    }
    return seasons;
  }

  useEffect(() => {
    // Загрузка списка сериалов
    if (!serials && !error) {
      axios
        .get('/api/serials')
        .then(({ data }) => addSerials(data))
        .then(() => setLoading(false))
        .catch(error => {
          console.log(error);
          setError(true);
          setLoading(false);
        });
    }

    // Определение сериала
    else if (serials && !serial) {
      const foundSerial = serials.find(serial => serial.name === serialName);
      setLoading(false);
      
      setSerial(foundSerial);
    }
  }, [serials, serial, addSerials, serialName, setError]);

  return (
    <div className="serial">
      {
        error ?
          <ErrorMessage message="Возникла ошибка при загрузке данных. Пожалуйста, перезагрузите страницу." /> :
          loading ?
            <Loading /> :
            <div>
              <Button className="mobile-button" green onClick={() => alert('Это еще не работает :(')}>
                Продолжить просмотр
              </Button>
              <section className="breadcrumbs">
                <Link to="/">Главная</Link>
                <span>-</span>
                <Link to={`/${serialName}`}>{serial.title}</Link>
              </section>
              <img src={serial.logo} className="logo" alt={serial.title} />
              <div className="mini-footer">
                <Button green onClick={() => alert('Это еще не работает :(')}>
                  Продолжить просмотр
                </Button>
              </div>
              <section className="all-seasons">
                <h2>Список сезонов:</h2>
                <div>
                  {
                    getSeasons(serial)
                  }
                </div>
              </section>
            </div>
      }
      
    </div>
  );
}

Serial.propTypes = {
  serialName: PropTypes.string.isRequired,
  serials: PropTypes.array,
  addSerials: PropTypes.func.isRequired
};

const mapStateToProps = ({ serials }) => ({ serials });

const mapDispatchToProps = dispatch => bindActionCreators({
  addSerials
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Serial);























/* const mapStateToProps = ({ serials, seasons }) => ({ serials, seasons });

const mapDispatchToProps = dispatch => bindActionCreators({
  addSerials,
  addSeasons
}, dispatch); */

//export default connect(mapStateToProps, mapDispatchToProps)(Serial);


/* function Serial(props) {
  // STATE
  const [serial, setSerial] = useState({});
  const [loading, setLoading] = useState(true);

  const {
    serial: currentSerial,
    serials,
    seasons,
    addSerials,
    addSeasons
  } = props;

  useEffect(() => {
    const foundSeason = seasons.find(season => season.title === currentSerial);
    //  Загрузка всех сериалов
    if (!serials) {
      axios
        .get('/db/serials.json')
        .then(({ data }) => addSerials(data))
        .catch(error => console.log(error));
    }
    // Если список сериалов есть, но сериал не определен, определяем
    else if (Object.keys(serial).length === 0) {
      const foundSerial = serials.find(serial => serial.name === currentSerial);
      setSerial(foundSerial);
    }

    // Загрузка сезонов если их нет
    else if (!foundSeason) {
      axios
        .get(`/db/serials/${currentSerial}.json`)
        .then(({ data }) => addSeasons(currentSerial, data))
        .then(() => setLoading(false))
        .catch(error => console.log(error));
    }

    else if (foundSeason) {
      setLoading(false)
    }
  });
  

  return (
    <div className="serial">
      <section className="breadcrumbs">
        <Link to="/">Главная</Link>
        <span>-</span>
        <Link to={`/${currentSerial}`}>{serial.title}</Link>
      </section>
      <img src={serial.logo} className="logo" alt={serial.title} />
      <div className="mini-footer" />
      
      <section className="seasons">
        {
          loading ?
            <Loading /> :
            <div>
              <section className="all-seasons">
                <h2>Список сезонов:</h2>
                <div>
                  {
                    seasons.find(season => season.title === currentSerial)
                    .seasons
                    .map(
                      (season, i) =>
                        <Link key={Math.random()} className="select-season-button" to={season[0].link}>Сезон {++i}</Link>
                    )
                  }
                </div>
              </section>
              {
                seasons.find(season => season.title === currentSerial)
                  .seasons
                  .map(
                    (season, i) =>
                      <article key={i} className="season">
                        <h1 className="title">Сезон {++i}</h1>
                        <section className="series">
                          {
                            season.map(seria => <Seria key={seria.seria} {...seria} />)
                          }
                        </section>
                      </article>
                  )
              }
            </div>
            
        }
      </section>
    </div>
  )
} */

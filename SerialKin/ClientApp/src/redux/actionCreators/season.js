import { SET } from '../actions/season';

export const setSeason = data => ({ type: SET, data });

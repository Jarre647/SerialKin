import { combineReducers } from 'redux';
import system from './system';
import serials from './serials';
import season from './season';

export default combineReducers({
  system,
  serials,
  season
});
import { SET } from '../actions/season';

const initialState = [];

function season(state = initialState, action) {
  switch (action.type) {
    case SET:
      return [...action.data];
    default: return state;
  }
}

export default season;

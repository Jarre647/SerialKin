﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SerialKin.Models;

namespace SerialKin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SerialsController : ControllerBase
    {
        private readonly SerialKinContext _context;

        public SerialsController(SerialKinContext context)
        {
            _context = context;
        }

        // GET: api/Serials
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Serials>>> GetSerials()
        {
            return await _context.Serials.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Seasons>> GetSeasons(int id)
        {
            if (_context.Serials.SingleOrDefault(x => x.Id == id) == null)
            {
                return NotFound();
            }
            var seasons = await _context.Seasons.Where(x => x.IdSerials == id).ToListAsync();

            if (seasons == null)
            {
                return NotFound();
            }
            return Ok(seasons);
        }
        [HttpGet("{id}/{idSeason}")]
        public async Task<ActionResult<Series>> GetSeries(int id, int idSeason)
        {
            if (_context.Serials.SingleOrDefault(x=>x.Id == id) == null)
            {
                return NotFound();
            }
            if (_context.Seasons.Where(x => x.Id == id)
                .SingleOrDefault(y => y.NameSeasons == Convert
                .ToString(idSeason)) == null)
            {
                return NotFound();
            }
            var series = await _context.Series.Where(x => x.IdSeason == idSeason).ToListAsync();
            if (series == null)
            {
                return NotFound();
            }
            return Ok(series);
        }

    }
}
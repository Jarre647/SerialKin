﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SerialKin.Models
{
    public class SerialKinContext : DbContext
    {
        public SerialKinContext (DbContextOptions<SerialKinContext> options)
            : base(options)
        {
        }

        public DbSet<SerialKin.Models.Serials> Serials { get; set; }
        public DbSet<SerialKin.Models.Seasons> Seasons { get; set; }
        public DbSet<Series> Series { get; set; }
    }
}

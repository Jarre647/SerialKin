﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SerialKin.Models
{
    public class Seasons
    {
        public int Id { get; set; }
        public int IdSerials { get; set; }
        public string NameSeasons { get; set; }
    }
}

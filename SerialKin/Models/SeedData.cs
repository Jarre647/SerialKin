﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
namespace SerialKin.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new SerialKinContext(
                serviceProvider.GetRequiredService<DbContextOptions<SerialKinContext>>()))
            {
                if (context.Serials.Any())
                {
                    return;
                }

                context.Serials.AddRange(
                    new Serials
                    {
                        Link = "/rick-and-morty",
                        Title = "Рик и Морти",
                        Cartoon = true,
                        Author = "cn",
                        Logo = "/images/rick-and-morty-logo.jpg",
                        Seasons = 3,
                        Image = "/images/rick-and-morty.jpg",
                        Name = "rick-and-morty"
                    },
                    new Serials
                    {

                        Link = "/simpsons",
                        Title = "Симпсоны",
                        Cartoon = true,
                        Author = "fox",
                        Logo = "/images/simpsons-logo.jpg",
                        Image = "/images/simpsons.jpg",
                        Seasons = 30,
                        Name = "simpsons"
                    },
                    new Serials
                    {
                        Link = "/family-guy",
                        Title = "Гриффины",
                        Cartoon = true,
                        Author = "fox",
                        Logo = "/images/family-guy-logo.jpg",
                        Image = "/images/family-guy.jpg",
                        Seasons = 17,
                        Name = "family-guy"
                    },
                    new Serials
                    {
                        Link = "/futurama",
                        Title = "Футурама",
                        Image = "/images/futurama.jpg",
                        Cartoon = true,
                        Author = "fox",
                        Logo = "/images/futurama-logo.jpg",
                        Seasons = 7,
                        Name = "futurama"
                    },
                    new Serials
                    {
                        Link = "/adventure-time",
                        Title = "Время приключений",
                        Image = "/images/adventure-time.jpg",
                        Cartoon = true,
                        Author = "cn",
                        Logo = "/images/adventure-time-logo.jpg",
                        Seasons = 10,
                        Name = "adventure-time"
                    },
                    new Serials
                    {
                        Link = "/disenchantment",
                        Title = "Разочарование",
                        Image = "/images/disenchantment.jpg",
                        Cartoon = true,
                        Author = "netflix",
                        Logo = "/images/disenchantment-logo.jpg",
                        Seasons = 1,
                        Name = "disenchantment"
                    },
                    new Serials
                    {
                        Link = "/south-park",
                        Title = "Южный парк",
                        Image = "/images/south-park.jpg",
                        Cartoon = true,
                        Author = "cc",
                        Logo = "/images/south-park-logo.jpg",
                        Seasons = 22,
                        Name = "south-park"
                    }
                    );
                context.SaveChanges();
            }

        }

        public static void Seasons(IServiceProvider serviceProvider)
        {
            using (var context = new SerialKinContext(
                serviceProvider.GetRequiredService<DbContextOptions<SerialKinContext>>()))
            {
                if (context.Seasons.Any())
                {
                    return;
                }

                context.Seasons.AddRange(
                new Seasons
                {
                    IdSerials = 1,
                    NameSeasons = "1"
                },
                new Seasons
                {
                    IdSerials = 1,
                    NameSeasons = "2"
                },
                new Seasons
                {
                    IdSerials = 1,
                    NameSeasons = "3"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "1"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "2"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "3"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "4"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "5"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "6"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "7"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "8"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "9"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "10"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "11"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "12"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "13"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "14"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "15"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "16"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "17"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "18"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "19"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "20"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "21"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "22"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "23"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "24"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "25"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "26"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "27"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "28"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "29"
                },
                new Seasons
                {
                    IdSerials = 2,
                    NameSeasons = "30"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "1"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "2"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "3"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "4"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "5"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "6"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "7"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "8"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "9"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "10"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "11"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "12"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "13"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "14"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "15"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "16"
                },
                new Seasons
                {
                    IdSerials = 3,
                    NameSeasons = "17"
                },
                new Seasons
                {
                    IdSerials = 4,
                    NameSeasons = "1"
                },
                new Seasons
                {
                    IdSerials = 4,
                    NameSeasons = "2"
                },
                new Seasons
                {
                    IdSerials = 4,
                    NameSeasons = "3"
                },
                new Seasons
                {
                    IdSerials = 4,
                    NameSeasons = "4"
                },
                new Seasons
                {
                    IdSerials = 4,
                    NameSeasons = "5"
                },
                new Seasons
                {
                    IdSerials = 4,
                    NameSeasons = "6"
                },
                new Seasons
                {
                    IdSerials = 4,
                    NameSeasons = "7"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "1"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "2"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "3"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "4"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "5"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "6"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "7"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "8"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "9"
                },
                new Seasons
                {
                    IdSerials = 5,
                    NameSeasons = "10"
                },
                new Seasons
                {
                    IdSerials = 6,
                    NameSeasons = "1"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "1"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "2"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "3"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "4"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "5"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "6"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "7"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "8"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "9"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "10"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "11"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "12"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "13"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "14"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "15"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "16"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "17"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "18"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "19"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "20"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "21"
                },
                new Seasons
                {
                    IdSerials = 7,
                    NameSeasons = "22"
                }
                );
                context.SaveChanges();
            }
        }
        public static void Series(IServiceProvider serviceProvider)
        {
            using (var context = new SerialKinContext(
                serviceProvider.GetRequiredService<DbContextOptions<SerialKinContext>>()))
            {
                if (context.Series.Any())
                {
                    return;
                }

                context.Series.AddRange(
                 new Series
                 {
                     IdSeason = 1,
                     NameSeries = "Пилотный выпуск",
                     NumberSeries = 1,
                     Link = "/rick-and-morty/1",
                     Image = "/images/rick-and-morty/1-1.jpg"
                 },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Пёс-Газонокосильщик",
                    NumberSeries = 2,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-2.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Анатомический парк",
                    NumberSeries = 3,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-3.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "М.НайтШьямал-Инопланетяне",
                    NumberSeries = 4,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-4.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Мисиксы крушат",
                    NumberSeries = 5,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-5.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Вакцина Рика №9",
                    NumberSeries = 6,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-6.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Растущий Газорпазорп",
                    NumberSeries = 7,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-7.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Рикнута славы",
                    NumberSeries = 8,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-8.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Надвигается нечто Риканутое",
                    NumberSeries = 9,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-9.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Поймать враждебного Рика из рода Рика",
                    NumberSeries = 10,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-10.jpg"
                },
                new Series
                {
                    IdSeason = 1,
                    NameSeries = "Задача Рика",
                    NumberSeries = 11,
                    Link = "/rick-and-morty/1",
                    Image = "/images/rick-and-morty/1-11.jpg"
                },
                new Series
                {
                    IdSeason = 65,
                    NumberSeries = 1,
                    NameSeries = "Разрикнутое время",
                    Link = "/rick-and-morty/2",
                    Image = "/images/rick-and-morty/2-1.jpg"
                },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 2,
                      NameSeries = "Морти-Освободитель",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 3,
                      NameSeries = "Автоматическая эротическая ассимиляция",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 4,
                      NameSeries = "Вспомрикнуть всё",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 5,
                      NameSeries = "Пора швифтануться",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 6,
                      NameSeries = "Рики, наверное, сошли с ума",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 7,
                      NameSeries = "Большой переполох в маленьком Санчесе",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 8,
                      NameSeries = "межпространственный кабель 2= Искушение судьбы",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 9,
                      NameSeries = "Посмотрим, кого зачистят на этот раз",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 65,
                      NumberSeries = 10,
                      NameSeries = "Шафер Сквуанчи",
                      Link = "/rick-and-morty/2",
                      Image = "/images/rick-and-morty/2-10.jpg"
                  },
                                new Series
                                {
                                    IdSeason = 3,
                                    NumberSeries = 1,
                                    NameSeries = "Побег из Рикшенка",
                                    Link = "/rick-and-morty/3",
                                    Image = "/images/rick-and-morty/3-1.jpg"
                                },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 2,
                      NameSeries = "Рикман с камнем",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 3,
                      NameSeries = "Маринованный Рик",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 4,
                      NameSeries = "Защитники 3= Возвращение в подземелье",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 5,
                      NameSeries = "Тайный дикий заговор",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 6,
                      NameSeries = "Отдых и Риклаксация",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 7,
                      NameSeries = "Риклантидическая путаница",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 8,
                      NameSeries = "Проветренный мозг Морти",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 9,
                      NameSeries = "Азбука Бэт",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 64,
                      NumberSeries = 10,
                      NameSeries = "Рикчжурский Мортидат",
                      Link = "/rick-and-morty/3",
                      Image = "/images/rick-and-morty/3-10.jpg"
                  },
                new Series
                {
                    IdSeason = 63,
                    NumberSeries = 1,
                    NameSeries = "Нью-Йорк против Гомера Симпсона",
                    Link = "/simpsons/9",
                    Image = "/images/simpsons/9-1.jpg"
                },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 2,
                      NameSeries = "Директор и нищий",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 3,
                      NameSeries = "Саксофон Лизы",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 4,
                      NameSeries = "Маленький домик ужасов на дереве 8",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 5,
                      NameSeries = "Семейное оружие",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 6,
                      NameSeries = "Барт - звезда",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 7,
                      NameSeries = "Две миссис Нахасапимапетилон",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 8,
                      NameSeries = "Лиза - скептик",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 9,
                      NameSeries = "Кусачая недвижимость",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 10,
                      NameSeries = "Волшебство на Вечнозелёной улице",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 11,
                      NameSeries = "Все поют, все танцуют",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 12,
                      NameSeries = "Карнавальный Барт",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 13,
                      NameSeries = "Радостная секта",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 14,
                      NameSeries = "Автобус",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 15,
                      NameSeries = "Последнее искушение Красти",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 16,
                      NameSeries = "Мошенничество со страховкой",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 17,
                      NameSeries = "Лиза Симпсон",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 18,
                      NameSeries = "Малыш Вигги",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 19,
                      NameSeries = "Симпсон на флоте",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 20,
                      NameSeries = "Проблемы с триллионами",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 21,
                      NameSeries = "Девчачье издание",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 22,
                      NameSeries = "Мусор Титанов",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-22.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 23,
                      NameSeries = "Царь Горы",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-23.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 24,
                      NameSeries = "Потерялась наша Лиза",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-24.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 25,
                      NameSeries = "Прирождённые целовальщики",
                      Link = "/simpsons/9",
                      Image = "/images/simpsons/9-25.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 1,
                      NameSeries = "Домик ужасов на дереве VII",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-1.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 2,
                      NameSeries = "Ты можешь переехать только дважды",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 3,
                      NameSeries = "Гомер боксёр",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 4,
                      NameSeries = "Бёрнс, сын Бёрнса",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 5,
                      NameSeries = "Барт в теневом бизнесе",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 6,
                      NameSeries = "Родители Милхауса разводятся",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 7,
                      NameSeries = "Лиза встречает свою судьбу",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 8,
                      NameSeries = "Ураган Нэдди",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 9,
                      NameSeries = "Таинственное путешествие нашего Гомера",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 10,
                      NameSeries = "Спрингфилдские материалы",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 11,
                      NameSeries = "Запутанный мир Мардж Симпсон",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 12,
                      NameSeries = "Гора безумия",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 13,
                      NameSeries = "Шери Боббинс",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 14,
                      NameSeries = "Шоу Щекотки, Царапки и Лайки",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 15,
                      NameSeries = "Фобия Гомера",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 16,
                      NameSeries = "Брат из другого сериала",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 17,
                      NameSeries = "Моя сестра, моя нянька",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 18,
                      NameSeries = "Гомер против восемнадцатой поправки",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 19,
                      NameSeries = "Степень школьной конфиденциальности",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 20,
                      NameSeries = "Собачий бунт",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 21,
                      NameSeries = "Старик и Лиза",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 22,
                      NameSeries = "Мы верим в Мардж",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-22.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 23,
                      NameSeries = "Враг Гомера",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-23.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 24,
                      NameSeries = "Продолжение Симпсонов",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-24.jpg"
                  },
                  new Series
                  {
                      IdSeason = 56,
                      NumberSeries = 25,
                      NameSeries = "Тайная война Лизы Симпсон",
                      Link = "/simpsons/8",
                      Image = "/images/simpsons/8-25.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 1,
                      NameSeries = "Кто стрелял в мистера Бёрнса? Часть II",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-1.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 2,
                      NameSeries = "Радиоактивный человек",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 3,
                      NameSeries = "Дом, милый дом",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 4,
                      NameSeries = "Барт продает свою душу",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 5,
                      NameSeries = "Лиза-вегетарианка",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 6,
                      NameSeries = "Маленький домик ужасов на дереве 6",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 7,
                      NameSeries = "Очень большой Гомер",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 8,
                      NameSeries = "Мать Симпсон",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 9,
                      NameSeries = "Последнее сияние Сайдшоу Боба",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 10,
                      NameSeries = "Симпсоны= 138 Специальный Выпуск",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 11,
                      NameSeries = "Мардж бы это не понравилось",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 12,
                      NameSeries = "Команда Гомера",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 13,
                      NameSeries = "Два плохих соседа",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 14,
                      NameSeries = "Сцены из классовой борьбы Спрингфилда",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 15,
                      NameSeries = "Барт — сыщик",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 16,
                      NameSeries = "Лиза — бунтарь",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 17,
                      NameSeries = "Гомер как Смитерс",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 18,
                      NameSeries = "День, когда умерло насилие",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 19,
                      NameSeries = "Рыбка по имени Сельма",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 20,
                      NameSeries = "Барт на дороге",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 21,
                      NameSeries = "22 коротких фильма о Спрингфилде",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 22,
                      NameSeries = "Разгневанный Эйб Симпсон и его недоделанный внук в «Проклятии Летающих Пираний»",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-22.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 23,
                      NameSeries = "Много Апу из ничего",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-23.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 24,
                      NameSeries = "Гомерпалуза",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-24.jpg"
                  },
                  new Series
                  {
                      IdSeason = 57,
                      NumberSeries = 25,
                      NameSeries = "Лето на пляже",
                      Link = "/simpsons/7",
                      Image = "/images/simpsons/7-25.jpg"
                  },
                 new Series
                 {
                     IdSeason = 59,
                     NumberSeries = 1,
                     NameSeries = "Квартет парикмахеров Гомера",
                     Link = "/simpsons/5",
                     Image = "/images/simpsons/5-1.jpg"
                 },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 2,
                      NameSeries = "Мыс страха",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 3,
                      NameSeries = "Гомер поступает в колледж",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 4,
                      NameSeries = "Бутон розы",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 5,
                      NameSeries = "Маленький домик ужасов на дереве 4",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 6,
                      NameSeries = "Мардж в бегах",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 7,
                      NameSeries = "Ребенок внутри Барта",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 8,
                      NameSeries = "Бойскауты в районе",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 9,
                      NameSeries = "Последнее искушение Гомера",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 10,
                      NameSeries = "$прингфилд (или Как я перестал бояться и полюбил легальные азартные игры)",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 11,
                      NameSeries = "Гомер и „Комитет бдительности“",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 12,
                      NameSeries = "Барт стал известным",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 13,
                      NameSeries = "Гомер и Апу",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 14,
                      NameSeries = "Лиза против Малибу Стейси",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 15,
                      NameSeries = "Гомер в глубоком космосе",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 16,
                      NameSeries = "Гомер любит Фландерса",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 17,
                      NameSeries = "Барт катается на слоне",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 18,
                      NameSeries = "Наследник Бёрнса",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 19,
                      NameSeries = "Билл Оэкли и Джош Вайнштейн",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 20,
                      NameSeries = "Мальчик, который слишком много знал",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 21,
                      NameSeries = "Возлюбленный леди Бувье",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 59,
                      NumberSeries = 22,
                      NameSeries = "Секреты успешного брака",
                      Link = "/simpsons/5",
                      Image = "/images/simpsons/5-22.jpg"
                  },
                 new Series
                 {
                     IdSeason = 60,
                     NumberSeries = 1,
                     NameSeries = "Лагерь Красти",
                     Link = "/simpsons/4",
                     Image = "/images/simpsons/4-1.jpg"
                 },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 2,
                      NameSeries = "Трамвай „Мардж“",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 3,
                      NameSeries = "Гомер-еретик",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 4,
                      NameSeries = "Лиза — королева красоты",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 5,
                      NameSeries = "Маленький домик ужасов на дереве 3",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 6,
                      NameSeries = "Щекотка и Царапка= Фильм",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 7,
                      NameSeries = "Мардж получает работу",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 8,
                      NameSeries = "Новый ребёнок в квартале",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 9,
                      NameSeries = "Мистер Плуг",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 10,
                      NameSeries = "Первое слово Лизы",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 11,
                      NameSeries = "Тройное шунтирование Гомера",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 12,
                      NameSeries = "Мардж против монорельса",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 13,
                      NameSeries = "Выбор Сельмы",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 14,
                      NameSeries = "Брат с той же планеты",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 15,
                      NameSeries = "Я люблю Лизу",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 16,
                      NameSeries = "Без „Даффа“",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 17,
                      NameSeries = "Последняя надежда Спрингфилда",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 18,
                      NameSeries = "Как это было= клип-шоу Симпсонов",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 19,
                      NameSeries = "Фронт",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 20,
                      NameSeries = "День изгнания",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 21,
                      NameSeries = "Оковы Мардж",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 60,
                      NumberSeries = 22,
                      NameSeries = "Сокращение Красти",
                      Link = "/simpsons/4",
                      Image = "/images/simpsons/4-22.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 1,
                      NameSeries = "Совершенно безумный папа",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-1.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 2,
                      NameSeries = "Мистер Лиза едет в Вашингтон",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 3,
                      NameSeries = "Когда Фландерс обанкротился",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 4,
                      NameSeries = "Барт-убийца",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 5,
                      NameSeries = "Гомер угадал",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 6,
                      NameSeries = "Какой отец, такой и клоун",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 7,
                      NameSeries = "Маленький домик ужасов на дереве 2",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 8,
                      NameSeries = "Пони для Лизы",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 9,
                      NameSeries = "Субботы грома",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 10,
                      NameSeries = "Горючий Мо",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 11,
                      NameSeries = "Бернс продаёт электростанцию",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 12,
                      NameSeries = "Я женился на Мардж",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 13,
                      NameSeries = "Радио Барта",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 14,
                      NameSeries = "Лиза - Грек",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 15,
                      NameSeries = "Гомер один",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 16,
                      NameSeries = "Барт-любовник",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 17,
                      NameSeries = "Гомер с битой",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 18,
                      NameSeries = "Разные призвания",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 19,
                      NameSeries = "Пёс смерти",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 20,
                      NameSeries = "Полковник Гомер",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 21,
                      NameSeries = "Чёрный вдовец",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 22,
                      NameSeries = "Шоу Отто",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-22.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 23,
                      NameSeries = "Друг Барта влюбился",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-23.jpg"
                  },
                  new Series
                  {
                      IdSeason = 61,
                      NumberSeries = 24,
                      NameSeries = "Братишка, не найдется ли у тебя пары десятицентовиков?",
                      Link = "/simpsons/3",
                      Image = "/images/simpsons/3-24.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 1,
                      NameSeries = "Барт получает двойку",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-1.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 2,
                      NameSeries = "Симпсон и Далила",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 3,
                      NameSeries = "Маленький домик ужасов на дереве",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 4,
                      NameSeries = "По две машины в каждом гараже и по три глаза каждой рыбе",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 5,
                      NameSeries = "Танцующий Гомер",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 6,
                      NameSeries = "Общество мертвых гольфистов",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 7,
                      NameSeries = "Барт против Дня благодарения",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 8,
                      NameSeries = "Барт сорвиголова",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 9,
                      NameSeries = "Щекотка и Царапка, и Мардж",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 10,
                      NameSeries = "Барта сбивает машина",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 11,
                      NameSeries = "Одна рыбка, две рыбки, рыба-собака, голубая рыбка",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 12,
                      NameSeries = "Наш путь",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 13,
                      NameSeries = "Гомер против Лизы и восьмой заповеди",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 14,
                      NameSeries = "Прекрасный директор",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 15,
                      NameSeries = "О, братишка, где же ты?",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 16,
                      NameSeries = "Собака Барта получает двойку",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 17,
                      NameSeries = "Старые деньги",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 18,
                      NameSeries = "Сила искусства",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 19,
                      NameSeries = "Замена Лизы",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 20,
                      NameSeries = "Война Симпсонов",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 21,
                      NameSeries = "Трое мужчин и комикс",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 62,
                      NumberSeries = 22,
                      NameSeries = "Кровная вражда",
                      Link = "/simpsons/2",
                      Image = "/images/simpsons/2-22.jpg"
                  },
                new Series
                {
                    IdSeason = 54,
                    NumberSeries = 1,
                    NameSeries = "Жир и танцы",
                    Link = "/simpsons/10",
                    Image = "/images/simpsons/10-1.jpg"
                },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 2,
                      NameSeries = "Волшебник Вечнозеленой аллеи",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 3,
                      NameSeries = "Барт — мама",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 4,
                      NameSeries = "Маленький домик ужасов на дереве 9",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 5,
                      NameSeries = "Когда ты захочешь стать выше звезд",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 6,
                      NameSeries = "Д’оу на ветру",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 7,
                      NameSeries = "Лиза получает пятёрку",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 8,
                      NameSeries = "Гомер Симпсон в фильме «Неприятности с почкой»",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 9,
                      NameSeries = "Охранник мэра",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 10,
                      NameSeries = "Вива Нед Фландерс",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 11,
                      NameSeries = "Непобедимый дикий Барт",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 12,
                      NameSeries = "Воскресенье, ужасное воскресенье",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 13,
                      NameSeries = "Гомер - Макс",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-13.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 14,
                      NameSeries = "Со мной Купидон",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-14.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 15,
                      NameSeries = "Мардж Симпсон в «Воплях сирени»",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-15.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 16,
                      NameSeries = "Освободите место Лизе",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-16.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 17,
                      NameSeries = "Максимум Гомердрайв",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-17.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 18,
                      NameSeries = "Библейские истории Симпсонов",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-18.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 19,
                      NameSeries = "Мама и Поп-Арт",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-19.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 20,
                      NameSeries = "Старик и троечник",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-20.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 21,
                      NameSeries = "Монти не может купить мне любовь",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-21.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 22,
                      NameSeries = "Они спасли мозг Лизы",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-22.jpg"
                  },
                  new Series
                  {
                      IdSeason = 54,
                      NumberSeries = 23,
                      NameSeries = "Тридцать минут над Токио",
                      Link = "/simpsons/10",
                      Image = "/images/simpsons/10-23.jpg"
                  },
                new Series
                {
                    IdSeason = 63,
                    NumberSeries = 1,
                    NameSeries = "Симпсоны готовят на открытом огне",
                    Link = "/simpsons/1",
                    Image = "/images/simpsons/1-1.jpg"
                },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 2,
                      NameSeries = "Барт Гений",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-2.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 3,
                      NameSeries = "Одиссея Гомера",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-3.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 4,
                      NameSeries = "Нет места позорней дома",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-4.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 5,
                      NameSeries = "Барт-генерал",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-5.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 6,
                      NameSeries = "Бедная Лиза",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-6.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 7,
                      NameSeries = "Зов Симпсонов",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-7.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 8,
                      NameSeries = "Голова-обличитель",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-8.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 9,
                      NameSeries = "Жизнь на быстрой дорожке",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-9.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 10,
                      NameSeries = "Вечеринка Гомера",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-10.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 11,
                      NameSeries = "Гроздья гнева",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-11.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 12,
                      NameSeries = "Красти арестован",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-12.jpg"
                  },
                  new Series
                  {
                      IdSeason = 63,
                      NumberSeries = 13,
                      NameSeries = "Один очаровательный вечер",
                      Link = "/simpsons/1",
                      Image = "/images/simpsons/1-13.jpg"
                  }

                );
                context.SaveChanges();
            }
        }
    }
}






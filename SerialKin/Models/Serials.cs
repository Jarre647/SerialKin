﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SerialKin.Models
{
    public class Serials
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public bool Cartoon { get; set; }
        public string Author { get; set; }
        public string Logo { get; set; }
        public int Seasons { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

    }
}

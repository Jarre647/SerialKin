﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SerialKin.Models
{
    public class Series
    {
        public int Id { get; set; }
        public int IdSeason { get; set; }
        public int  NumberSeries { get; set; }
        public string NameSeries { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
    }
}
